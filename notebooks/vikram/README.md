# Senior Design Lab Notebook - Vikram Belthur
## 2/10/2022
Today we finished and submitted our project proposal. I specifically focussed on completing the Visual Aid, documenting the project's solution and on the server subsystem. 

## 2/11/2022
Today I met with the machine shop to discuss aspects of Carbon Control, specifically, how we couls integrate their services to create properly sized holes for LEDs. They also suggested that I should consider how the battery should be placed in the 3D enclosure, along with other considerations for the charging wire, etc. 

## 2/17/2022
Today we selected parts for our project. This included an ESP8266 MCU, a Panasonic PIR sensor and a Sensirion SCD41 air quality sensor. We also decided to use an amplifier with our MCU along with a speaker. 

## 2/20/2022
Today we had a work session for our design document. I worked on the server subsystem and the alarm subsystem. I validated some part selections I made earlier, such as the amplifier/DAC, and the CO2 sensor. The original PIR sensor we picked was changed by Mois to a newer one which is wall mounted. In the design document also started update the block diagran, and talk about the cost of the project in term of parts and labor. I prepared schematics for the PIR sensor, the CO2 sensor, and the MCU in KiCad for use in the design document. 

## 2/21/2022
Today was the design document check with the professor. We had to improve our RV section to be in keeping with the course requirements. Mois and I worked on the soldering assignment. 

## 2/24/2022
Today we continued out work on the design document. I was responsible for the creation of the Visual Aid and Physical design which was incorporated into out final document that we submitted tonight. I was working on the alarm subsystem and choosing the parts, while describing in detail how it operated in the design document. We ended up inculding an amplifier in our document because we neeed to drive an external speaker. We also needed to choose an amp with an on-board DAC since it had to output digital I2S data to a real sound. Some of the components, such as the MCU were changed as to make the project more economical. In the end I countributed around 45% of the design document, with Mois contributing another 45-55%. 

![Visual Aid](visualaid.PNG)
![Visual Aid](PD.PNG)

## 3/01/2022
Today we had our formal design review in front of Prof. Fliflet. I made a powerpoint presentation for our design review. The prof. specifically asked us many questions relating to the ACH and us being able to accuately measure CO2 concentration decay. Specifically, he wanted to know if the sensor would be sensitive enough. Mois and I worked on a set of use cases for the device so that we could better understand what our design had to do. This was rolled into the appendix for our design document. 

## 3/07/2022
Today I ordered preliminary parts such as a Senserion SCD41 CO2 sensor as well as a set of 3 ESP32 development boards. After spring break we can begin to write code for the dev-kits and server subsystems while we wait for our board to be fabricated internationally. We made a design choice to change the USB - UART converter to a CP210N chip since that (the older model) was in stock. 

## 3/18/2022
We worked on a joint call with Mois while he was on KiCad sharing his screen with us so that we could finalize our design and start to work on our board's layout.  We were on this call on facebook messenger video chat. We tried to finish working on our MCU subsystem, specifically on the question of how to program our board. This was difficult because there was short supply of some components on DigiKey and Mouser. 

![Visual Aid](usb.PNG)

## 3/19/2022
Today we went back into the messenger call for more design work, this time for part of the layout for the board. An interesting problem I had to solve was to figure our what the controlled impedences should be for the differential USB traces (D+ and D-). I ended up using the tool provided by JLCPCB (the vendor we will use to order the board). This gave us the width of the trace to use. For the power subsystem I calculated which resistors values had to be used in the design, when it relates to the boost converter. 

![la](p.PNG)

## 3/20/2022
The team gathered for a messenger call again to complete our work in designing our board. The call was done in the same manner as before where Mois was hosted the meeting and we joined with his screen shared. I assisted in helping Mois layout the board. I helped in design for manufacturability (DFM) related issues for the layout. 

![la](layout1.PNG)

I found this layout to be interesting and unique but it seems to me that there is a lot of wasted space. 

## 3/24/2022
Today Mois and I started to work on the ESP32 dev kits and the server subsystem. We hope to be abl to paralellize this project so that we can continue our momentum while waiting for the boards to come in. I was able to connect the MCU to a 2.4 GHz WiFi network, and Mois was able to build a preliminary folder. We can see an IP adress below for the MCU. We were also able to get our CO2 sensor working with the dev-kit. Tanmay and I completed this task. 

![la](ip.PNG)

## 3/29/2022
Today we redesigned the layout of out board so that it would be more compact, with much less wasted space than in our previous iteration of the board. 

## 3/30/2022
I completed the IPR and submitted it. 

## 3/31/2022
Board arrived today from JCLPCB along with the stencil. Mois and I opened the packaging and inspected the contents. 

## 4/1/2022
Today we soldered the first revision of our board in the lab using the reflow oven, stencil, and soldering paste. Mois and I got the board ready, cleaned it, applied the paste, placed the parts etc. David instructed us on how to operate the reflow oven. 

![la](ob.PNG)

## 4/08/2022
Today was an interesting day in the lab because of board bring-up. Unfortunately Mois and Tanmay got to have all the fun in setting up the Rev.1 board because suddenly our CO2 sensor stopped working. Data exchange was not happening on the I2C Bus, and I was struggling to debug it. Eventually we came to the conclusion that the sensor itself was broken and we ordered three extras. 

## 4/14/2022
I was debugging some small issues with the board and its jumpers (this week I had a major midterm so I was not as active in the project work). I figured out that Jumper 2 needed to be open and Jumper 3 needed to be closed in order to ensure proper functionality of the board. I will be out of town this weekend. I also worked on getting the speakers to produce sound with the amplifier today. 

## 4/21/2022 - 4/22/2022
Today we prepared for the mock demo thorough the night. Mois and I driled the enclosure with tools we purchased from the local hardware store. Mois and I worked on soldering the REV.2 version of our board (the black new one). We ended up creating two copies of the board for this prohect. Tanmay worked on calculating the ACH value. Today we also tried to acheive integration between al of our various subsystems. This was broadly succesful and we were able to show that our project was mostly working to Daniel on that Friday. 

![la](nb.PNG)

## 4/24/2022 - 4/25/2022
For these two days (and through the night) we worked to make sure our project was fully ready to be demoed. This entailed drilling a new box and re-packaging the product. There was an unexpected issue with the speaker, but I resolved this since it was a simple electrical issue. Luckilly all issues were solved before our demo and we had an interesting and succesful demo for Prof. Fliflet. Circle K has good coffee at 3:30 AM for all-nighters. 

## 4/28/2022
Today we prepared a mock presentation and showed it to the comms. TA. SHe was really helpful in some of her tips and we plan on incorperating her advice, particularly about eye contact, reading off of the slides, and looking backwards/sideways at the screen, in our final presenations. 

## 5/03/2022
We got suited up and presented our project to applause and cheers (jk). In all seriousness, our presentation was a success and we were able to deliver a well rehersed explanation of our project. 

## 5/04/2022
The report is finished. ECE 445 is over. It is done.