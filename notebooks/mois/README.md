# 2-10-22

Our project proposal was due on this day. We met as a partial team, Vikram and I, to discuss the state of our draft. Tanmay participated via chat. Our objective was to complete and submit our proposal. We sketched a rough outline of how our project would be structure from a functional block POV. We also discussed whether the MCU should be an STM32 or whether it should be a ESP32/8266. We recognized that we had initially declared in our RFA and proposal draft that we would include an STM32 for compute and an ESP32 for wifi connectivity. We decided to simplify the design and reduce part cost by using just the ESP32 or the ESP8266. This was not reflected in our proposal to avoid a last minute overhaul. Additional proofing and editing was done

# 2-17-22
My principal objective was part selection. Vikram and I met in person. Our goal was to move our abstract blocks to a concrete design. The objective of the day was to identify suitable parts and a rough design / schematic for our sensor node. I researched commercially available CO2 sensors that were reasonably priced, as well as what requirements we need to satisfy our use cases. I also did the prelimary work about our server solution.

# 2-20-22
Vikram, Tanmay, and I met to discuss our proposal score and identify improvement to be made as well as align on the design. The meeting was conducted virtually over messenger. We agreed on an outline of what each block needed to accomplish as well as parts that would likely satisfy them. We divided ownership of the design document sections in accordance with the preference of each member. Afterwards, I wrote our tolerance analysis with an emphasis on the accuracy and response time sensitivity. I also provided feedback to rough drafts from Vikram. I finally researched the server subsystem and acceptable hosting, libraries, and storage options. I have proposed Heroku hosting due to cost and ease of use. I will ask for feedback from my groupmates.

# 2-21-22
We had a design document check today. We presented for Prof. Fliflet. Feedback focused on requirements and verifications. I also worked with vikram on the soldering assignment.

# 2-24-22
Our objective for the session was to continue the design document. A major question I worked on today was how to charge our battery. I considered various cell packs with multi-cell configs to have high amperage and high capacity. I first considered the TPS4056 bms, but found the whole 2s configuration with a balance lead untenable. I then pivoted to to a single cell configuration and selected the mcp73831. There are a few variants but I selected the 4.2V B variant to drive the LED. Vikram was also there to assist with writing the design document. In the end, I wrote approximately 45% of the design document, with Vikram contributing another 45%. The whole design doc is a record of what was done today. I include certain excerpts.

![Block Diagram](imgs/224-blk.jpeg)

I also worked on the tolerance analysis of our device.

![Tolerance Diagram](imgs/224-tl.png)

# 3-1-22
Today, we had our design review. We recieved alot of good feedback for our design and feel like we are on the right track. We also further refined our use cases included on our design document. The purpose of this is to understand the use cases our project must satisfy. I did this with Vikram. 

![UC Diagram](imgs/31-uc.png)


# 3-7-22
Today, Vikram and I held a working group meeting to refine our design. Vikram worked on parts availablity, while I checked and refined schematics. I mostly worked on validating the schematics in the design document as comporting with the datasheets. I found few in accuracies. I also surveyed PCB manufacturers and available services for stenciling.

An example refinement is below (old and new)
![Pre Diagram](imgs/37-amp_pre.png)
![Post Diagram](imgs/37-amp_post.png)

# 3-13-22
Had an exchange with Daniel about team deliverables concerning PCB ordering.

# 3-18-22
Today was a joint PCB designing session with Vikram and Tanmay. I designed on Kicad while my team mates participated over messenger. Vikram was an active participant, Tanmay contributed as well.

The goal was the design of the MCU and USB to UART sections. We experience part shortages here and needed to be nimble. We essentially re-started the design.

We concluded on the following design:
![MCU](imgs/318-MCU.png)

# 3-19-22
We refined our USB to UART design as there are many signalling lines. We very much wanted our board to be easily programmable so we included an auto_programming circuit. There are many signals to control the USB connection and this took effort to resolve. Vikram worked on the PCB stackup and controlled impedance traces. Tanmay considered the amplifier.
![MCU](imgs/318-USB.png)

We also finalized our alarm and sensor subsystems.

Vikram computed values for power subsystem. This system was radically changed from design doc implementation.

# 3-20-22
The goal of today was layout, routing, and ordering of the PCB. I worked on layout of components within our board. The 4 layers were selected to be 2x signal and pwr and gnd. Vikram assisted with manufacturer specifications and DFM. Tanmay was present. 

A notable difficulty is routing the differential pair controlled impedance trace.

![layout](imgs/320_layout.png)

The board was ordered today.

# 3-24-22
We still await our board. A primary objective is getting our server operational with the dev-kit. Our first task was getting the MCU to connect with a wifi network. I developed a server and vikram worked on connecting the MCU to wifi. 

![wifi](imgs/324_wifi.png)
Here I include a picture of our ESP being assigned an IP address as a record of our achievement.

Vikram and Tanmay got the CO2 sensor operational.

# 3-29-22
With Tanmay and Vikram, design revision 2 of our PCB and Layoyt. We kept the schematic identical as we had not tested it yet. The layout is more compact. Tanmay assisted with layout. Vikram assisted with routing.

![board](imgs/329-board.png)


# 3-30-22
Worked on IPR. This was done individually. I described various design changes and also what challenges we faced.

![IPR](imgs/330-IPR.png)

# 3-31-22
Board arrived today with Stencil. This is a key milestone as we faced significant delays.


# 4-1-22
Today we reflow soldered the board to achieve a good electromechanical connection between our componenets and board. 

Vikram and I prepared the board, applied paste, placed components. 

Tanmay worked on fixing the CO2 sensor that mysteriously broke down a few days ago. 



# 4-8-22
Today we power up our board for the first time. We slowly injected voltage to see if there was a short. Tanmay and I observed that our MOSFET footprint was mirrored. To resolve this we shorted drain and source and cut a trace to correct the issue.

![Board](imgs/41-board.png)

# 4-15-22
Today, we were able to verify our battery charging for the first time. This involved discharging a battery and monitoring it's recharge rates. Tanmay and I worked on this together. I also confirmed that the speakers were operational, though the file format was unconventional.

![Batt](imgs/415-batt.png)

# 4-22-22
Prepared for mock demo by drilling enclosure with Vikram Belthur. We also worked on our server to get the ACH and CO2 graphs to display. Vikram and I also soldered the new layout together. Finally, Tanmay worked on the ACH computation. 
We also soldered our first board of the second revision. We did two at once for maximum efficiency.

![enc](imgs/422-encl.png)

# 4-25-22
Prepared for demo and demo-ed with the team. Tanmay worked on organizing our videos for RV verification. Vikram worked on debugging a speaker issue. I also began data collection at 6am and monitored for issues. Final server bugs were corrected during this time. 

![data](imgs/425-data.png)

# 4-28-22
Prepared Mock Presentation with Vikram Belthur. Tanmay Goyal was present for the first hour. Slides followed provided emplate and content was written by us. We descibed the success and failures of our project. It was mostly successes. We presented to a communications TA and recieved feedback.

![pres](imgs/428-pres.png)

# 5-3-22
Updated presentation based on feedback and did multiple run throughs with the whole team. The objective was to polish the presentation and make our project clear to a lay audience. We presented to professor Fliflet.
![pres](imgs/53-pres.png)

# 5-4-22
Today we wrote our final report. The process was divided by subsystems amongst the group members. The final report catalogued our successes and challenges. The report was detailed. I wrote the power and mcu subsystem descriptions and verifications. Tanmay edited. Vikram wrote the various sections of the alarm, and sensor subsystem. 

It can be viewed on the website.
